#include "InputManager.h"

#include <GL/freeglut.h>

#include "glm/gtc/constants.hpp"

#include "utils/opengl/camera/FreeCamera.h"

#include <iostream>

/**
* @brief The singleton get instance method
*
* @return The instance
*/
InputManager& InputManager::getInstance()
{
	static InputManager instance;
	return instance;
}

/**
* @brief When a key is released
*
* @param key The key
*/
void InputManager::keyUp(unsigned char key, int x, int y) {

	getInstance().keyPressed[key] = false;

	glutPostRedisplay();
}

/**
 * @brief When a key is pressed but doesn't need real time reaction
 *
 * @param key The key
 */
void InputManager::keyPress(unsigned char key, int x, int y)
{
	getInstance().keyPressed[key] = true;

	if (getInstance().keyPressed[27]) {
		if (getInstance().inMenu) {
			glutSetCursor(GLUT_CURSOR_INHERIT);
			glutPassiveMotionFunc(mouseMove);
			getInstance().inMenu = false;
		}
		else {
			glutSetCursor(GLUT_CURSOR_NONE);
			glutPassiveMotionFunc(mouseMoveFPS);
			getInstance().inMenu = true;
		}
	}

	glutPostRedisplay();
}

/**
 * @brief Process user keyboard inputs.
 *
 */
void InputManager::processKeyInputs()
{
	// Camera movements
	if (InputManager::getInstance().keyPressed['z'] && InputManager::getInstance().keyPressed['q'])	// Diagonal up left
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::FORWARD_LEFT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['z'] && InputManager::getInstance().keyPressed['d'])	// Diagonal up right
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::FORWARD_RIGHT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['s'] && InputManager::getInstance().keyPressed['q'])	// Diagonal down left
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::BACKWARD_LEFT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['s'] && InputManager::getInstance().keyPressed['d'])	// Diagonal down right
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::BACKWARD_RIGHT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['z'])
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::FORWARD, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['s'])
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::BACKWARD, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['q'])
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::LEFT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['d'])
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::RIGHT, Program::getInstance().deltaTime);
	else
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::NONE, Program::getInstance().deltaTime);
}

/**
 * @brief Mouse movement callback on FPS mode
 *
 * @param x X position
 * @param y Y position
 */
void InputManager::mouseMoveFPS(int x, int y)
{
	float xoffset = x - getInstance().mousePositionX;
	float yoffset = getInstance().mousePositionY - y; // reversed since y-coordinates range from bottom to top

	// For rotation when the mouse is close to an edge
	float detectionLimitY = Program::getInstance().windowHeight * 0.2f;
	float detectionLimitX = Program::getInstance().windowWidth * 0.2f;

	getInstance().mousePositionX = static_cast<float>(x);
	getInstance().mousePositionY = static_cast<float>(y);

	// Process the movement with the camera
	Program::getInstance().camera->ProcessMouseMovement(xoffset, yoffset, Program::getInstance().deltaTime);

	glutPostRedisplay();

	// Placing the mouse back to the middle of the screen if at the edges
	float centerX = Program::getInstance().windowHeight / 2.0f;
	float centerY = Program::getInstance().windowHeight / 2.0f;

	if (x < detectionLimitX || x > Program::getInstance().windowWidth - detectionLimitX) {  //you can use values other than 100 for the screen edges if you like, kind of seems to depend on your mouse sensitivity for what ends up working best
		getInstance().mousePositionX = centerX;   //centers the last known position, this way there isn't an odd jump with your cam as it resets
		getInstance().mousePositionY = centerY;
		glutWarpPointer((int)centerX, (int)centerY);  //centers the cursor
	}
	else if (y < detectionLimitY || y > Program::getInstance().windowHeight - detectionLimitY) {
		getInstance().mousePositionX = centerX;
		getInstance().mousePositionY = centerY;
		glutWarpPointer((int)centerX, (int)centerY);
	}
}