#include "VertexBufferLayout.h"

#include <GL/glew.h>

#include <glm/glm.hpp>

#include <vector>
#include <algorithm>

void VertexBufferLayout::pushf(unsigned int count, unsigned int index)
{
	m_elements.push_back({ GL_FLOAT, count, index, false });
	m_stride += count * getSizeOfElement(GL_FLOAT);
}

void VertexBufferLayout::pushui(unsigned int count, unsigned int index)
{
	m_elements.push_back({ GL_UNSIGNED_INT, count, index, false });
	m_stride += count * getSizeOfElement(GL_UNSIGNED_INT);
}

void VertexBufferLayout::pushuc(unsigned int count, unsigned int index)
{
	m_elements.push_back({ GL_UNSIGNED_BYTE, count, index, true });
	m_stride += count * getSizeOfElement(GL_UNSIGNED_BYTE);
}

void VertexBufferLayout::pushvec2f(unsigned int count, unsigned int index)
{
	m_elements.push_back({ GL_FLOAT, count * 2, index, false });
	m_stride += count * 2 * getSizeOfElement(GL_FLOAT);
}

void VertexBufferLayout::pushvec3f(unsigned int count, unsigned int index)
{
	m_elements.push_back({ GL_FLOAT, count * 3, index, false });
	m_stride += count * 3 * getSizeOfElement(GL_FLOAT);
}

void VertexBufferLayout::pushvec4f(unsigned int count, unsigned int index)
{
	m_elements.push_back({ GL_FLOAT, count * 4, index, false });
	m_stride += count * 4 * getSizeOfElement(GL_FLOAT);
}

/**
 * @brief Sorts the elements by their position in the layout (to avoid holes)
 *
 */
void VertexBufferLayout::sortElementsByPosition()
{
	std::sort(m_elements.begin(), m_elements.end(),
		[](VertexBufferLayoutElement a, VertexBufferLayoutElement b)
		{
			return (a.index < b.index);
		});
}

/**
 * @brief Get the GL type size of an element (a type)
 *
 * If we add new items types that can be added to the layout, we must
 * complete this method with the new possible types
 *
 * @param type the GL type
 * @return The size of it
 */
unsigned int VertexBufferLayout::getSizeOfElement(unsigned int type)
{
	switch (type)
	{
	case GL_FLOAT:			return 4;
	case GL_UNSIGNED_INT:	return 4;
	case GL_UNSIGNED_BYTE:	return 1;
	case GL_FLOAT_VEC2:		return sizeof(glm::vec2);
	case GL_FLOAT_VEC3:		return sizeof(glm::vec3);
	case GL_FLOAT_VEC4:		return sizeof(glm::vec4);
	default:
		break;
	}

	return 0;
}