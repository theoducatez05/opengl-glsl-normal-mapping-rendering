#include "VertexArray.h"

#include <vector>
#include <iostream>
#include <memory>

#include <GL/glew.h>

#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Mesh.h"
#include "VertexBufferLayout.h"

VertexArray::VertexArray()
{
	glGenVertexArrays(1, &m_bufferId);
}

VertexArray::~VertexArray()
{
	// See ref count
	if (m_bufferId != 0) {
		glDeleteVertexArrays(1, &m_bufferId);
		std::cout << "VertexArray : " << m_bufferId << " deleted" << std::endl;
	}
}

/**
 * @brief Links the vertex array, the index buffer and the vertex buffer together (data layout)
 *
 * @param vertexBuffer
 * @param indexBuffer
 * @param vertexBufferLayout
 */
void VertexArray::link(const std::shared_ptr<VertexBuffer> vertexBuffer, const std::shared_ptr<IndexBuffer> indexBuffer, VertexBufferLayout& vertexBufferLayout)
{
	// Bind the vertex array and the vertex buffer
	bind();
	vertexBuffer->bind();
	if (indexBuffer) {
		indexBuffer->bind();
	}

	// Sort the layout by position and check if all indexes are filled
	vertexBufferLayout.sortElementsByPosition();
	const std::vector<VertexBufferLayoutElement> elements = vertexBufferLayout.getElements();
	// If not, the layout can't be used
	if (elements.back().index + 1 != elements.size()) {
		std::cout << "ERROR::VERTEX_ARRAY::ALL_ATTRIBUTE_NOT_IN_LAYOUT : The layout attributes are not all defined (gaps between attributes still present)" << std::endl;
		exit(1);
	}

	unsigned int offset = 0;

	// Add the data layout to the vertex Array from the VertexBufferLayout
	for (unsigned int i = 0; i < elements.size(); i++) {
		const VertexBufferLayoutElement element = elements[i];
		glEnableVertexAttribArray(element.index);
		glVertexAttribPointer(element.index, element.count, element.type, element.normalized, vertexBufferLayout.getStride(), reinterpret_cast<void*>(offset));
		offset += element.count * VertexBufferLayout::getSizeOfElement(element.type);
	}

	vertexBuffer->unbind();
	unbind();
}

void VertexArray::bind() const
{
	glBindVertexArray(m_bufferId);
}

void VertexArray::unbind() const
{
	glBindVertexArray(0);
}