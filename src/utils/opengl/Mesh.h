#pragma once
/*****************************************************************//**
 * @file   Mesh.h
 * @brief  The mesh class and vertex struct
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General includes
#include <vector>
#include <memory>

// GLM includes
#include <glm/glm.hpp>

// Program includes
#include "Texture.h"
#include "Shader.h"
#include "VertexBuffer.h"
#include "VertexArray.h"
#include "Material.h"

/**
 * @brief Representation of a vertex
 */
struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 textCoordinates;
    glm::vec3 tangent;
};

/**
 * @brief Representation of a Mesh
 */
class Mesh
{
private:
    // mesh data
    std::vector<Vertex>                     m_vertices;
    std::vector<unsigned int>               m_indices;
    std::vector<std::shared_ptr<Texture>>   m_textures;
    Material                                m_material;

    //  render data
    std::shared_ptr<VertexBuffer>           m_vertexBuffer;
    std::shared_ptr<VertexArray>            m_vertexArray;
    std::shared_ptr<IndexBuffer>            m_indexBuffer;

public:

    /**
     * @brief Constructor
     *
     * \param vertices The vertices
     * \param indices The indices
     * \param textures The textures
     * \param material THe material
     */
    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<std::shared_ptr<Texture>> textures, Material material);

    /**
     * @brief Draw the mesh with Phong shader uniforms
     *
     * @param shader The shader
     * @param lit If the mesh is supposed to be lit
     */
    void drawPhong(const std::shared_ptr <Shader> shader, bool lit);
    /**
     * @brief Draw the mesh with Torrence shader uniforms
     *
     * @param shader The shader
     * @param lit If the mesh is supposed to be lit
     */
    void drawTorrence(const std::shared_ptr <Shader> shader, bool lit);
    /**
     * @brief Draw the mesh with Minnaert shader uniforms
     *
     * @param shader The shader
     * @param lit If the mesh is supposed to be lit
     */
    void drawMinnaert(const std::shared_ptr <Shader> shader, bool lit);
    /**
     * @brief Draw the mesh with Toon shader uniforms
     *
     * @param shader The shader
     * @param lit If the mesh is supposed to be lit
     */
    void drawToon(const std::shared_ptr <Shader> shader, bool lit);
    /**
     * @brief Draw the mesh without setting any shader uniforms
     *
     * @param shader The shader
     * @param lit If the mesh is supposed to be lit
     */
    void drawSimple(const std::shared_ptr <Shader> shader);
    /**
     * @brief Draw the mesh with Phong realistic shader uniforms
     *
     * @param shader The shader
     * @param lit If the mesh is supposed to be lit
     */
    void drawPBR(const std::shared_ptr <Shader> shader, bool lit);

    // Getters and setters
    inline const std::shared_ptr <VertexBuffer> getVertexBuffer() const { return m_vertexBuffer; };
    inline const std::shared_ptr<VertexArray> getVertexArray() const { return m_vertexArray; };
    inline const std::shared_ptr<IndexBuffer> getIndexBuffer() const { return m_indexBuffer; };
    inline Material& getMaterial() { return m_material; }

    inline const std::vector<std::shared_ptr<Texture>>& getTextures() const { return m_textures; };

private:
    /**
     * @brief Sets up the mesh buffers
     *
     */
    void setupMesh();
};

