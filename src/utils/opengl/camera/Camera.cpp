#include "Camera.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

/**
 * @brief Constructor
 *
 * \param initialPosition Initial position of the camera
 * \param up The UP vector
 * \param yaw The initial yaw
 * \param pitch The initial Pitch
 */
Camera::Camera(glm::vec3 position, glm::vec3 up, float yaw, float pitch)
{
    m_position = position;
    m_worldUp = up;
    m_yaw = yaw;
    m_pitch = pitch;
    updateCameraVectors();
}

/**
 * .brief Returns the view matrix calculated using Euler Angles and the LookAt Matrix
 *
 * \return The view matrix
 */
glm::mat4 Camera::GetViewMatrix()
{
    return glm::lookAt(m_position, m_position + m_front, m_up);
}

/**
 * @brief Updates the camera vectors
 *
 */
void Camera::updateCameraVectors()
{
    // calculate the new Front vector
    glm::vec3 front;
    front.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
    front.y = sin(glm::radians(m_pitch));
    front.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
    m_front = glm::normalize(front);

    // Also re-calculate the Right and Up vector
    m_right = glm::normalize(glm::cross(m_front, m_worldUp)); // normalize the vectors, not to slow down the movement
    m_up = glm::normalize(glm::cross(m_right, m_front));      // normalize the vectors, not to slow down the movement
}