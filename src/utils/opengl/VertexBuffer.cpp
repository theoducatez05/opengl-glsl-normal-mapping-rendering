#include "VertexBuffer.h"

// General includes
#include <iostream>

// GL includes
#include <GL/glew.h>

/**
* @brief Constructor
*
* \param numVertices The number of vertices
* \param vertices THe vertices
* \param verticeSize One vertex size
*/
VertexBuffer::VertexBuffer(size_t numVertices, const void* vertices, size_t verticeSize) :
	m_numVertices(numVertices)
{
	// Generate the vertex buffer
	glGenBuffers(1, &m_bufferId);
	// Binding the buffer
	glBindBuffer(GL_ARRAY_BUFFER, m_bufferId);
	// Create the space needed for our data in the buffer
	glBufferData(GL_ARRAY_BUFFER, verticeSize * m_numVertices, vertices, GL_STATIC_DRAW);
}

VertexBuffer::~VertexBuffer()
{
	if (m_bufferId != 0) {
		glDeleteBuffers(1, &m_bufferId);
		std::cout << "Vertexbuffer : "<<m_bufferId << " deleted" << std::endl;
	}
}

void VertexBuffer::bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, m_bufferId);
}

void VertexBuffer::unbind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}