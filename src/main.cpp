/*****************************************************************//**
 * @file   main.cpp
 * @brief  The main program file
 *
 * Assignement 3 - Real Time Rendering - Trinity College 2021/2022
 *
 * @author theod
 * @date   February 2022
 *********************************************************************/

// General includes
#include <iostream>
#include <vector>
#include <math.h>
#include <iomanip>
#include <sstream>

// GL includes
#include <GL/glew.h>
#include <GL/freeglut.h>

// glm includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// imgui
#include "utils/imgui/imgui.h"
#include "utils/imgui/imgui_impl_glut.h"
#include "utils/imgui/imgui_impl_opengl3.h"

// Project includes
#include "utils/opengl/camera/Camera.h"
#include "utils/opengl/Shader.h"
#include "utils/opengl/GraphicObject.h"
#include "utils/opengl/camera/FreeCamera.h"
#include "utils/InputManager.h"
#include "utils/opengl/Skybox.h"

// Objects
std::shared_ptr<GraphicObject> cube;

// Textures for IMGUI
std::unique_ptr<Texture> textureMountainPreview;
std::unique_ptr<Texture> textureSnowPreview;
std::unique_ptr<Texture> textureBeachPreview;
std::unique_ptr<Texture> textureChurchPreview;
std::unique_ptr<Texture> textureNiagaraPreview;

// Textures Preview
std::unique_ptr<Texture> textureRockPreview;
std::unique_ptr<Texture> textureWoodPreview;
std::unique_ptr<Texture> textureMetalPreview;

void showImGui() {
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGLUT_NewFrame();

	// Light stuff
	ImGui::Begin("Theo Ducatez RR 3");

	ImGui::Text("Light properties:");
	ImGui::Dummy(ImVec2(0.0f, 10.0f));

	static float lightPosition[] = { cube->getTransform().getPosition().x, cube->getTransform().getPosition().y, cube->getTransform().getPosition().z };
	ImGui::SliderFloat3("position", lightPosition, -5.0, 5.0);
	static float lightColor[4] = { 1.0f,1.0f,1.0f,1.0f };
	ImGui::Dummy(ImVec2(0.0f, 7.0f));
	ImGui::ColorEdit3("color", lightColor);

	cube->getTransform().setPosition({ lightPosition[0], lightPosition[1], lightPosition[2] });
	Program::getInstance().cubeLightShader->bind();
	Program::getInstance().cubeLightShader->setUniform3fv("u_color", { lightColor[0], lightColor[1], lightColor[2] });

	ImGui::Dummy(ImVec2(0.0f, 10.0f));
	ImGui::Separator();

	// Shader stuff
	ImGui::Dummy(ImVec2(0.0f, 10.0f));
	ImGui::Text("Shader properties:");
	ImGui::Dummy(ImVec2(0.0f, 10.0f));

	static float roughness = 0.5f;
	static float metallic = 0.0f;
	static float ambientOclusion = 1.0f;
	static float ambientStrength = 0.05f;

	ImGui::SliderFloat("Roughness", &roughness, 0.0f, 1.0f);
	ImGui::Dummy(ImVec2(0.0f, 7.0f));
	ImGui::SliderFloat("Metallic", &metallic, 0.0f, 1.0f);
	ImGui::Dummy(ImVec2(0.0f, 7.0f));
	ImGui::SliderFloat("Ambient Occlusion", &ambientOclusion, 0.0f, 1.0f);
	ImGui::Dummy(ImVec2(0.0f, 7.0f));
	ImGui::SliderFloat("Ambient Strength", &ambientStrength, 0.0f, 1.0f);

	ImGui::Dummy(ImVec2(0.0f, 10.0f));
	ImGui::Separator();

	ImGui::Dummy(ImVec2(0.0f, 10.0f));
	ImGui::Text("Objects:");
	ImGui::Dummy(ImVec2(0.0f, 10.0f));

	const char* items[] = { "sphere", "teapot"};
	static int item_current_idx = 0; // Here we store our selection data as an index.

	if (ImGui::BeginListBox("##listbox 1", ImVec2(-FLT_MIN, 2 * ImGui::GetTextLineHeightWithSpacing())))
	{
		for (int n = 0; n < IM_ARRAYSIZE(items); n++)
		{
			const bool is_selected = (item_current_idx == n);
			if (ImGui::Selectable(items[n], is_selected))
				item_current_idx = n;
			Program::getInstance().currentObjectType = items[item_current_idx];

			// Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
			if (is_selected)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndListBox();
	}

	ImGui::Dummy(ImVec2(0.0f, 10.0f));
	ImGui::Separator();

	// Textures
	ImGui::Dummy(ImVec2(0.0f, 10.0f));
	ImGui::Text("Textures:");
	ImGui::Dummy(ImVec2(0.0f, 10.0f));

	ImVec2 size = ImVec2(64.0f, 64.0f);                     // Size of the image we want to make visible
	ImVec2 uv0 = ImVec2(0.0f, 0.0f);                        // UV coordinates for lower-left
	ImVec2 uv1 = ImVec2(1.0f, 1.0f);		// UV coordinates for (32,32) in our texture
	ImVec4 bg_col = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);         // Black background
	ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);       // No tint

	if (ImGui::ImageButton((ImTextureID)textureRockPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		if (Program::getInstance().currentObjectType == "sphere") {
			Program::getInstance().currentObject = Program::getInstance().sphere;
		}
		else {
			Program::getInstance().currentObject = Program::getInstance().teapot;
		}

		roughness = 0.3f;
		metallic = 0.0f;
		ambientOclusion = 0.5f;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureWoodPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		if (Program::getInstance().currentObjectType == "sphere") {
			Program::getInstance().currentObject = Program::getInstance().sphere3;
		}
		else {
			Program::getInstance().currentObject = Program::getInstance().teapot3;
		}

		roughness = 0.6f;
		metallic = 0.0f;
		ambientOclusion = 0.5f;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureMetalPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		if (Program::getInstance().currentObjectType == "sphere") {
			Program::getInstance().currentObject = Program::getInstance().sphere2;
		}
		else {
			Program::getInstance().currentObject = Program::getInstance().teapot2;
		}

		roughness = 0.3f;
		metallic = 0.85f;
		ambientOclusion = 0.5f;
	}

	ImGui::Dummy(ImVec2(0.0f, 10.0f));
	ImGui::Separator();

	// Skyboxes
	ImGui::Dummy(ImVec2(0.0f, 10.0f));
	ImGui::Text("Skyboxes:");
	ImGui::Dummy(ImVec2(0.0f, 10.0f));

	if (ImGui::ImageButton((ImTextureID)textureMountainPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureSnowPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox5;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureChurchPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox2;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureBeachPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox3;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureNiagaraPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox4;
	}

	ImGui::End();

	Program::getInstance().PBRShader->bind();
	Program::getInstance().PBRShader->setUniform3fv("u_light.color", { lightColor[0], lightColor[1], lightColor[2] });
	Program::getInstance().PBRShader->setUniform3fv("u_light.position", cube->getTransform().getPosition());
	Program::getInstance().PBRShader->setUniform1f("u_ambientStrength", ambientStrength);
	Program::getInstance().PBRShader->setUniform1f("u_ao", ambientOclusion);
	Program::getInstance().PBRShader->setUniform1f("material.metalness", metallic);
	Program::getInstance().PBRShader->setUniform3fv("material.roughness1.color", { roughness, roughness, roughness });

	// Rendering
	ImGui::Render();
	ImGuiIO& io = ImGui::GetIO();
	glViewport(0, 0, (GLsizei)io.DisplaySize.x, (GLsizei)io.DisplaySize.y);
	glUseProgram(0);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

/**
 * @brief Glut main diplsay method (used in the main glut loop)
 *
 */
void display()
{
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	cube->drawSimple(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());

	Program::getInstance().PBRShader->bind();
	Program::getInstance().PBRShader->setUniform3fv("u_viewPos", Program::getInstance().camera->getPosition());
	Program::getInstance().PBRShader->unbind();

	Program::getInstance().currentObject->drawPBR(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());

	Program::getInstance().currentSkybox->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());

	showImGui();

	glutSwapBuffers();
}

/**
 * @brief Objects and shader initialization function
 *
 */
void init()
{
	glEnable(GL_DEPTH_TEST);

	// Set up the light object in the scene and its shader
	Program::getInstance().cubeLightShader = std::make_shared<Shader>(
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Light.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Light.frag.glsl");
	cube = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Cube\\Cube.obj", Program::getInstance().cubeLightShader, false);
	cube->getTransform().setPosition({ -1.0f, 1.0f, -2.0f });
	cube->getTransform().setScale({ 0.05f, 0.05f, 0.05f });
	cube->setLit(false);

	// Set up the skybox
	Program::getInstance().skybox = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Mountains\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().skybox2 = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Church\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().skybox3 = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Beach\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().skybox4 = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Niagara\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().skybox5 = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Snow\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().currentSkybox = Program::getInstance().skybox;

	// Load gui textures
	textureMountainPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Mountains\\front.jpg").c_str(), TextureType::Diffuse, false);
	textureBeachPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Beach\\front.jpg").c_str(), TextureType::Diffuse, false);
	textureSnowPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Snow\\front.jpg").c_str(), TextureType::Diffuse, false);
	textureNiagaraPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Niagara\\front.jpg").c_str(), TextureType::Diffuse, false);
	textureChurchPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Church\\front.jpg").c_str(), TextureType::Diffuse, false);

	textureMetalPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\Objects\\Metal\\Metal021_4K_Color.jpg").c_str(), TextureType::Diffuse, false);
	textureWoodPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\Objects\\Wood\\Planks020_4K_Color.jpg").c_str(), TextureType::Diffuse, false);
	textureRockPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\Objects\\Stone\\PavingStones070_4K_Color.jpg").c_str(), TextureType::Diffuse, false);

	// Set up the Phong Shader
	Program::getInstance().PBRShader = std::make_shared<Shader>(
		Program::getInstance().RESOURCE_FOLDER + "shaders\\PBR.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\PBR.frag.glsl");
	Program::getInstance().PBRShader->bind();
	Program::getInstance().PBRShader->setUniform3fv("u_light.color", { 1.0f, 1.0f, 1.0f });
	Program::getInstance().PBRShader->setUniform3fv("u_light.position", cube->getTransform().getPosition());
	Program::getInstance().PBRShader->setUniform3fv("u_viewPos", Program::getInstance().camera->getPosition());


	// Set up objects
	Program::getInstance().sphere = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Sphere\\Sphere.fbx", Program::getInstance().PBRShader, false);
	Program::getInstance().sphere2 = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Sphere\\Sphere2.fbx", Program::getInstance().PBRShader, false);
	Program::getInstance().sphere3 = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Sphere\\Sphere3.fbx", Program::getInstance().PBRShader, false);

	Program::getInstance().teapot = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Teapot\\Teapot.fbx", Program::getInstance().PBRShader, false);
	Program::getInstance().teapot2 = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Teapot\\Teapot2.fbx", Program::getInstance().PBRShader, false);
	Program::getInstance().teapot3 = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Teapot\\Teapot3.fbx", Program::getInstance().PBRShader, false);

	Program::getInstance().currentObject = Program::getInstance().sphere;
}

/**
 * @brief Process a window resize event
 *
 * @param width The new window width
 * @param height The new window height
 */
void resizeWindow(int width, int height)
{
	InputManager::getInstance().mousePositionX = width / 2.0f;
	InputManager::getInstance().mousePositionY = height / 2.0f;

	Program::getInstance().windowHeight = height;
	Program::getInstance().windowWidth = width;

	glViewport(0, 0, width, height); // reset the viewport
	Program::getInstance().projection = glm::perspective(glm::radians(45.0f), (float)Program::getInstance().windowWidth / Program::getInstance().windowHeight, 0.1f, 5000.0f);
}

/**
 * @brief Glut idle function callback, to update the scene.
 *
 */
void updateScene()
{

	// Update program deltaTime
	Program::getInstance().updateDeltaTime((long)glutGet(GLUT_ELAPSED_TIME));

	// Rotations of the objects
	Program::getInstance().currentObject->getTransform().setRotationY(Program::getInstance().currentObject->getTransform().getRotationY() + Program::getInstance().deltaTime * .5f);

	// Process key inputs
	InputManager::getInstance().processKeyInputs();

	// Draw the next frame
	glutPostRedisplay();
}

/**
 * @brief Main function
 *
 * @param argc System arguments count
 * @param argv System arguments
 */
int main(int argc, char** argv)
{
	// Set up the window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(Program::getInstance().windowWidth, Program::getInstance().windowHeight);
	glutCreateWindow("CS7GV03 | Assignment 3 | Theo Ducatez");

	GLenum res = glewInit();

	// Check for any errors
	if (res != GLEW_OK) {
		std::cout << "Error: \n" << glewGetErrorString(res) << std::endl;
		return 1;
	}

	// Set up input configurations and callbacks
	//glutSetCursor(GLUT_CURSOR_NONE);
	glutKeyboardFunc(InputManager::keyPress);
	glutKeyboardUpFunc(InputManager::keyUp);
	//glutPassiveMotionFunc(InputManager::mouseMoveFPS);

	// Register general call back funtions
	glutDisplayFunc(display);
	glutIdleFunc(updateScene);
	glutReshapeFunc(resizeWindow);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();

	// Setup Platform/Renderer backends
	ImGui_ImplGLUT_Init();
	ImGui_ImplGLUT_InstallFuncs();
	ImGui_ImplOpenGL3_Init("#version 330");

	// Setting up the graphic objects
	init();

	// Begin infinite event loop
	glutMainLoop();

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGLUT_Shutdown();
	ImGui::DestroyContext();

	return 0;
}