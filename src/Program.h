#pragma once
/*****************************************************************//**
 * @file   Program.h
 * @brief  General singleton program class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General includes
#include <memory>

// GL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Program includes
#include "utils/opengl/camera/FreeCamera.h"
#include "utils/opengl/GraphicObject.h"
#include "utils/opengl/Skybox.h"

/**
 * @brief The program singelton class
 */
class Program
{
private:

    /**
     * @brief The program singleton initialization
     *
     * It initializes the coordinate map
     */
    Program(){};
    ~Program() {};
    Program(const Program&) {};
    const Program& operator=(const Program&) {};

    static Program* instance;

public:

    /**
     * @brief Get the unique instance
     *
     * @return The program instance
     */
    static Program& getInstance();

    /**
     * @brief Refreshes the unique singleton instance
     *
     * Used when the game reloads
     */
    static void refreshInstance();

    // Window data
    int windowWidth = 1280;
    int windowHeight = 720;

    // Constant Folders
    std::string RESOURCE_FOLDER = "D:\\Etudes\\TCD\\Courses\\RR\\RR-Assigment_3\\resources\\";

    // Deltatime
    float deltaTime = 0.0f;
    long lastFrame = 0;

    // Camera
    std::unique_ptr<Camera> camera = std::make_unique<FreeCamera>(glm::vec3(0.70f, 1.f, -3.3f), glm::vec3(0, 1, 0));

    // Projection
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)windowWidth / windowHeight, 0.1f, 5000.0f);

    // Shaders
    std::shared_ptr<Shader> transmittanceShader;
    std::shared_ptr<Shader> PBRShader;
    std::shared_ptr<Shader> cubeLightShader;

    // Objects
    std::shared_ptr<GraphicObject> teapot;
    std::shared_ptr<GraphicObject> teapot2;
    std::shared_ptr<GraphicObject> teapot3;
    std::shared_ptr<GraphicObject> sphere;
    std::shared_ptr<GraphicObject> sphere2;
    std::shared_ptr<GraphicObject> sphere3;

    std::string currentObjectType = "sphere";

    // Current object
    std::shared_ptr<GraphicObject> currentObject;

    // Skybox
    std::shared_ptr<Skybox> skybox;
    std::shared_ptr<Skybox> skybox2;
    std::shared_ptr<Skybox> skybox3;
    std::shared_ptr<Skybox> skybox4;
    std::shared_ptr<Skybox> skybox5;

    std::shared_ptr<Skybox> currentSkybox;

    /**
     * @brief Updates the program deltaTime
     *
     * @param elapsedTime Time between two frames
     */
    void updateDeltaTime(long elapsedTime);
};

