#version 330

const float PI = 3.14159265359f;

struct	ColorData {
	bool isTexture;

	vec3 color;
	sampler2D texture;
};

struct	Material {
	ColorData diffuse1;
	ColorData normal1;
	ColorData roughness1;

	float metalness;
};

struct Light {
    vec3 position;

    vec3 color;
};

in vec2 textureCoordinates;
in vec3 FragPos;
in vec3 normal;
in vec3 tangentLightPos;
in vec3 tangentViewPos;
in vec3 tangentFragPos;

uniform Material material;
uniform Light u_light;

uniform vec3 u_viewPos;
uniform float u_ambientStrength = 0.1;
uniform float u_ao = 1.0;

out vec4 FragColor;

/*
	Function that calculate the normal distribution function (D) in the equation

	Params:
		- N : vec3 : The normal
		- H : vec3 : The H parameter
        - roughness: float : The roughness

	Return:
		- The normal distribution
*/
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

/*
	Function that calculate the geometry distribution with Schlick GGX method

	Params:
		- NdotV : float : NdotV
		- Roughness : float : The roughness

	Return:
		- GGX term
*/
float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}

/*
	Function that calculate the geometry function (G) in the equation

	Params:
		- N : vec3 : The normal
		- V : vec3 : The view direction
		- L : vec3 : The L term
		- float : roughness : The roughness

	Return:
		- The geometry distribution (G) in the equation
*/
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

/*
	Function that calculate the Fresnel ratio giving Theta the incident angle and F0

	Params:
		- cosTheta : float : The incident angle (with the normal and the view direction)
		- F0 : float : The F0 value

	Return:
		- The Fresnel ratio of reflectance (1 - this to have the refracted one)
*/
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}

void main() {

	// Get the diffuse color
	vec3 diffuseColor;
	if (material.diffuse1.isTexture) {												// If texture is a texture
		diffuseColor = texture(material.diffuse1.texture, textureCoordinates).rgb;
	}
	else {																			// If material color
		diffuseColor = material.diffuse1.color;
	}

	// Retrieving normals from texture
	vec3 norm;
	if (material.normal1.isTexture) {												// If texture is a texture
		norm = vec3(texture(material.normal1.texture, textureCoordinates)).rgb;
	}
	else {																			// If material color
		norm = material.normal1.color;
	}
	norm = normalize(norm * 2.0 - 1.0);

	// Retrieving the roughness from texture
	float roughness;
	if (material.roughness1.isTexture) {												// If texture is a texture
		roughness = vec3(texture(material.roughness1.texture, textureCoordinates)).y;
	}
	else {																				// If material color
		roughness = material.roughness1.color.y;
	}

	// Light direction calculation
	vec3 lightDir = normalize(tangentLightPos - tangentFragPos);
	// Camera direction vector calculation
	vec3 viewDir = normalize(tangentViewPos - tangentFragPos);
	// Relfection direction calculation
	vec3 reflectDir = reflect(-lightDir, norm);

    // Fresnel reflecrance calculation for F0
	vec3 F0 = vec3(0.04);
    F0 = mix(F0, diffuseColor, material.metalness);

    // reflectance equation
    vec3 Lo = vec3(0.0);

    // calculate per-light radiance
    vec3 L = normalize(tangentLightPos - tangentFragPos);
    vec3 H = normalize(viewDir + L);
    float distance    = length(tangentLightPos - tangentFragPos);
    float attenuation = 1.0;//1.0 / (distance * distance);
    vec3 radiance     = u_light.color * attenuation;

    // cook-torrance brdf
    float NDF = DistributionGGX(norm, H, roughness);
    float G   = GeometrySmith(norm, viewDir, L, roughness);
    vec3 F    = fresnelSchlick(max(dot(H, viewDir), 0.0), F0);

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - material.metalness;

    // Final calculation
    vec3 numerator    = NDF * G * F;
    float denominator = 4.0 * max(dot(norm, viewDir), 0.0) * max(dot(norm, L), 0.0) + 0.0001;
    vec3 specular     = numerator / denominator;

    // add to outgoing radiance Lo
    float NdotL = max(dot(norm, L), 0.0);
    Lo += (kD * diffuseColor / PI + specular) * radiance * NdotL;

    // Calculating the ambient lighting
    vec3 ambient = u_ambientStrength * diffuseColor * u_ao;
    vec3 color = ambient + Lo;

    // Gamma correction
    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));

    FragColor = vec4(color, 1.0);
}