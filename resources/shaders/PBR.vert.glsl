#version 330

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_textureCoordinates;
layout (location = 3) in vec3 a_tangent;

struct Light {
    vec3 position;

    vec3 color;
 };

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform mat4 u_parentModel;

uniform vec3 u_viewPos;
uniform Light u_light;

out vec2 textureCoordinates;
out vec3 normal;
out vec3 FragPos;
out vec3 tangentLightPos;
out vec3 tangentViewPos;
out vec3 tangentFragPos;

void main()
{
    // Vertex position
    gl_Position = u_projection * u_view * u_parentModel * u_model * vec4(a_position, 1.0);

    // Fragment position
    FragPos = vec3(u_model * vec4(a_position, 1.0));

    // Texture coordinates
    textureCoordinates = a_textureCoordinates;

    // Calculating the TBN matrix
    mat3 normalMatrix = transpose(inverse(mat3(u_model)));
    vec3 T = normalize(normalMatrix * a_tangent);
    vec3 N = normalize(normalMatrix * a_normal);
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);

    mat3 TBN = transpose(mat3(T, B, N));

    // Applying TBN to the other uniform vectors
    // So that we have more performances
    tangentLightPos = TBN * u_light.position;
    tangentViewPos  = TBN * u_viewPos;
    tangentFragPos  = TBN * vec3(u_model * vec4(a_position, 1.0));

    //normal = mat3(transpose(inverse(u_model))) * a_normals;
}
